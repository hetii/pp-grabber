phantom.onError = function(msg, trace) {
  var msgStack = ['PHANTOM ERROR: ' + msg];
  if (trace && trace.length) {
    msgStack.push('TRACE:');
    trace.forEach(function(t) {
      msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
    });
  }
  console.log(msgStack.join('\n'));
  phantom.exit(1);
};

var page = require('webpage').create(),
  system = require('system');
  var track_number = system.args[1];

  /*
  page.onConsoleMessage = function(msg) {
    console.log(msg);
  };
  page.onResourceReceived = function(response) {
      if (response.stage !== "end") return;
      console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + response.url);
  };
  page.onResourceRequested = function(requestData, networkRequest) {
      console.log('Request (#' + requestData.id + '): ' + requestData.url);
  };
  page.onUrlChanged = function(targetUrl) {
      console.log('New URL: ' + targetUrl);
  };
  page.onLoadFinished = function(status) {
      console.log('Load Finished: ' + status);
  };
  page.onLoadStarted = function() {
      console.log('Load Started');
  };
  page.onNavigationRequested = function(url, type, willNavigate, main) {
      console.log('Trying to navigate to: ' + url);
  };
  */

  page.onError = function(msg, trace) {
    var msgStack = ['ERROR: ' + msg];
    if (trace && trace.length) {
      msgStack.push('TRACE:');
      trace.forEach(function(t) {
        msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function +'")' : ''));
      });
    }
    console.error(msgStack.join('\n'));
  };

  page.open('https://emonitoring.poczta-polska.pl/?numer='+track_number, function() {
    page.settings.resourceTimeout = 10000; // Avoid freeze!!!
    page.settings.dpi = "96";
    page.viewportSize = {width: 1920, height: 1080};
    // page.viewportSize = {width: 2048, height: 1080};
    // page.paperSize = {width: 1024, height: 768};
    page.paperSize = {
      format: 'A3',
      orientation: 'portrait'
    }

    page.evaluate(function() {
      $('.close').click();
      $('#cookiesBarClose').click();
      $('#BSzukajO').click();

    function process(rule) {
      if(rule.cssRules) {
        for(var i=rule.cssRules.length-1; i>=0; i--) {
          process(rule.cssRules[i]);
        }
      }
      if(rule.type == CSSRule.MEDIA_RULE) {
        if(window.matchMedia(rule.media.mediaText).matches) {
          rule.media.mediaText = "all";
        } else {
          rule.media.mediaText = "not all";
        }
      }
      return rule;
    }
    for(var i=0; i<document.styleSheets.length; i++) {
      process(document.styleSheets[i]);
    }
    });
    // page.zoomFactor = 0.50;
    window.setTimeout(function () {
      console.log('Render the page!');
      page.render(system.args[2], { quality: '100'});
      phantom.exit();
    }, 4000); // Change timeout as required to allow sufficient time
});
