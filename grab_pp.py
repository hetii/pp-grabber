import os
import sys
import csv
import signal
import datetime
import subprocess
import pandas as pd
from zeep import Client
from datetime import timedelta
from zeep.wsse.username import UsernameToken
from functools import wraps
import errno

class TimeoutError(Exception):
    pass

def run_command(args):
        # a bug in phantomjs: hang randomly
        # http://code.google.com/p/phantomjs/issues/detail?id=652
        def timeout_handler(signum, frame):
            error_message=os.strerror(errno.ETIME)
            raise TimeoutError(error_message)

        timelimit = 6
        handler = signal.signal(signal.SIGALRM, timeout_handler)
        signal.alarm(timelimit)
        try:
            phantomjs = subprocess.Popen(args)
            phantomjs.communicate()
        except TimeoutError:
            phantomjs.kill()
            return args
        finally:
            # reset the handler to the old one
            signal.signal(signal.SIGALRM, handler)
        # cancel the alarm
        signal.alarm(0)

def parse_xlsx_old(file_name, sheet_name):
    df = pd.read_excel(file_name, sheet_name=sheet_name)
    # e=pd.DataFrame(df)
    c = df.to_csv()
    data = c.split('\n')[2:]
    # print(data)
    x = csv.reader(data, delimiter=',')
    name = ""
    address = ""
    number = ""
    ret = []
    for item in x:
        if not item: continue
        if item[1]:
            if number.strip():
                name = name.replace('  ', '').strip()
                # address = address.replace('  ', '').strip()
                number = number.replace('  ', '').strip()
                ret.append((name, address, number))
            name = ""
            address = ""
            number = ""
        name += item[2] 
        address += item[3]
        number +=  item[4].split(' ')[0]
    name = name.replace('  ', '').strip()
    # address = address.replace('  ', '').strip()
    number = number.replace('  ', '').strip()

    ret.append((name, address, number))
    return ret

def parse_xlsx(file_name, sheet_name):
    df = pd.read_excel(file_name, sheet_name=sheet_name)
    # e=pd.DataFrame(df)
    c = df.to_csv()
    data = c.split('\n')[2:]
    # print(data)
    x = csv.reader(data, delimiter=',')
    name = ""
    address = ""
    number = ""
    ret = []
    for item in x:
        if not item: continue
        print(item)
        name = item[4].strip()
        address = ''
        number = "00"+item[1].strip()
        ret.append((name, address, number))
    # for item in x:
    #     if not item: continue
    #     if item[3]:
    #         if number.strip():
    #             name = name.replace('  ', '').strip()
    #             # address = address.replace('  ', '').strip()
    #             number = number.replace('  ', '').strip()
    #             ret.append((name, address, number))
    #         name = ""
    #         address = ""
    #         number = ""
    #     name += item[3]
    #     address += ""
    #     number +=  item[0].split(' ')[0]
    #name = name.replace('  ', '').strip()
    # address = address.replace('  ', '').strip()
    #number = number.replace('  ', '').strip()

    #ret.append((name, address, number))
    return ret

class PocztaPolska(object):
    

    url='https://tt.poczta-polska.pl/Sledzenie/services/Sledzenie?wsdl'

    def __init__(self, username='sledzeniepp', password='PPSA'):
        self._client = Client(self.url, wsse=UsernameToken(username, password))

    def check_number(self, nr):
        return self._client.service.sprawdzPrzesylkePl(numer=nr)

    def get_shipment_details(self, res):
        if res.danePrzesylki.dataNadania: 
            dataNadania = res.danePrzesylki.dataNadania.strftime('%Y-%m-%d')
        else:
            dataNadania = '0000-00-00'

        nazwa = getattr(res.danePrzesylki.urzadNadania, 'nazwa', '')
        if not res.danePrzesylki.urzadNadania:
            # print (f"Missing urzadNadania for {res}")
            return
        nrLokalu = res.danePrzesylki.urzadNadania.daneSzczegolowe.nrLokalu

        address = res.danePrzesylki.urzadNadania.daneSzczegolowe.ulica + " "
        address += res.danePrzesylki.urzadNadania.daneSzczegolowe.nrDomu
        address += "/" + nrLokalu if nrLokalu else ""
        address += ", "
        address += res.danePrzesylki.urzadNadania.daneSzczegolowe.pna + " "
        address += res.danePrzesylki.urzadNadania.daneSzczegolowe.miejscowosc

        ret = {}
        ret['number'] = res.numer
        ret['date_of_posting'] = dataNadania
        ret['type'] = res.danePrzesylki.rodzPrzes
        ret['country_of_origin'] = res.danePrzesylki.krajNadania
        ret['country_of_destination'] = res.danePrzesylki.krajPrzezn
        ret['office_of_posting'] = nazwa + f" ({address})"
        ret['weight'] = res.danePrzesylki.masa
        ret['service_terminated'] = res.danePrzesylki.zakonczonoObsluge

        return ret

    def get_shipment_events(self, res):
        l = []
        for event in getattr(res.danePrzesylki.zdarzenia, 'zdarzenie', []):
            d = {}
            d['name'] = event.nazwa
            d['datetime'] = event.czas
            d['postal_unit'] = event.jednostka.nazwa
            d['last_one'] = event.konczace
            l.append(d)
        return l

def generate_csv_file(file_name, sheet_name):
    pp = PocztaPolska()
    # status = pp.check_number('00559007734755451430')
    # print(status)    
    xlsx = parse_xlsx(file_name, sheet_name)
    data = {'Numer:':[], 'Nadano:':[], 'Dostarczono:':[], 'Termin:':[], 'Firma:':[]}
    for name, address, number in xlsx:
        #print(f"Numer: {number} -> {name}"), 
        status = pp.check_number(number)
        sd = pp.get_shipment_details(status)
        if not sd:
            print(f"Invalid sd result for {number} -> {name}- skipping!")
            continue
        datetime_str = "******NIE*******"
        deadline = "***BRAK***"
        if sd['service_terminated']:
            se = pp.get_shipment_events(status)
            for x in se:
                if x['last_one']:
                    datetime_str = x['datetime']
                    date_time_obj = datetime.datetime.strptime(datetime_str, '%Y-%m-%d %H:%M')
                    deadline = (date_time_obj+timedelta(days=7)).strftime('%Y-%m-%d')
        data['Numer:'].append(number)
        data['Nadano:'].append(sd['date_of_posting'])
        data['Dostarczono:'].append(datetime_str)
        data['Termin:'].append(deadline)
        data['Firma:'].append(name)
        print(f"Numer: {number}, Nadano: {sd['date_of_posting']}, Dostarczono: {datetime_str}, Termin: {deadline}, Firma: {name}")
    df = pd.DataFrame(data=data)
    os.makedirs(sheet_name, exist_ok=True)
    df.to_csv(f"./{sheet_name}/raport.csv", sep=',', index=False)
    print()
    print(f"Found {len(data['Numer:'])} numbers!")

def generate_pdf_file(file_name, sheet_name):
    xlsx = parse_xlsx(file_name, sheet_name)
    pdf_path = f"{sheet_name}/pdf"
    os.makedirs(pdf_path, exist_ok=True)
    for name, address, number in xlsx:
        #print(name, address, number)
        cmd = ['/usr/bin/phantomjs', 'grab_pdf.js', number, f"./{pdf_path}/{name} - {number}.pdf"]
        while cmd:
            cmd = run_command(cmd)
            if cmd:
                print(f"Retry: {cmd}")
    print(f"Files that should be generated: {len(xlsx)}")

if __name__ == '__main__':
    file_name = 'listy polecone.xlsx'
    #sheet_name= '30.08'
    #sheet_name= '02.09'
    #sheet_name= '02.09_2'
    sheet_name= 'aa'
    #sheet_name= '04.09'
    #generate_csv_file(file_name, sheet_name)
    generate_pdf_file(file_name, sheet_name)
    # 00659007734682235674
    #   559007734755453335